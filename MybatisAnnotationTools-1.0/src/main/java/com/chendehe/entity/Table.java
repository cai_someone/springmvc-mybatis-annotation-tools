package com.chendehe.entity;

import java.util.List;
import java.util.Map;

import lombok.Data;

/**
 * @author CDH
 * @since 2019/7/29 11:01
 */
@Data
public class Table {
    private String modelPackage;
    private String modelFolder;
    private String modelPrefix;
    private String modelSuffix;

    private String voPrefix;
    private String voSuffix;
    private String voPackage;
    private String voFolder;

    private String servicePrefix;
    private String serviceSuffix;
    private String servicePackage;
    private String serviceFolder;

    private String implPrefix;
    private String implSuffix;
    private String implPackage;
    private String implFolder;

    private String controllerPrefix;
    private String controllerSuffix;
    private String controllerPackage;
    private String controllerFolder;

    private String daoPackage;
    private String daoFolder;
    private String daoPrefix;
    private String daoSuffix;


    private String tablePrefix;


    /**
     * K:tableName#comment V:fields
     */
    private Map<String, List<Field>> tableFields;
}
