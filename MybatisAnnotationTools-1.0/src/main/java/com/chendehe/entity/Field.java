package com.chendehe.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author CDH
 * @since 2019/7/29 11:01
 */
@Getter
@Setter
@NoArgsConstructor
public class Field {

    private String fieldName;

    private String fieldType;

    private String javaFieldName;

    private String remarks;

    private boolean key;

    private int precision;

    private int scale;

    private boolean nullable;

    public String getRemarks() {
        return nullable ? remarks + " (Not Null)" : remarks;
    }
    public String getJdbcType(){
        if (fieldType.toUpperCase().startsWith("DATETIME")){
            return "TIMESTAMP";
        }
        if (fieldType.toUpperCase().startsWith("INT")){
            return "INTEGER";
        }
        if (fieldType.toUpperCase().startsWith("VARCHAR")){
            return "VARCHAR";
        }if (fieldType.toUpperCase().startsWith("DECIMAL")){
            return "DECIMAL";
        }if (fieldType.toUpperCase().startsWith("CHAR")){
            return "VARCHAR";
        }if (fieldType.toUpperCase().startsWith("DATE")){
            return "DATE";
        }if (fieldType.toUpperCase().startsWith("LONG")){
            return "VARCHAR";
        }if (fieldType.toUpperCase().startsWith("TINYINT")){
            return "INTEGER";
        }if (fieldType.toUpperCase().startsWith("BIGINT")){
            return "INTEGER";
        }
        throw new RuntimeException("javaType not found! fieldType is " + fieldType);
    }

    public String getJavaType() {
        if (fieldType.toUpperCase().startsWith("VARCHAR2")) {
            return "String";
        }if (fieldType.toUpperCase().startsWith("BIGINT")) {
            return "Integer";
        }
        if (fieldType.toUpperCase().startsWith("CHAR")) {
            return "String";
        }
        if (fieldType.toUpperCase().startsWith("CLOB")) {
            return "String";
        }
        if (fieldType.toUpperCase().startsWith("DATE")) {
            return "Date";
        }
        if (fieldType.toUpperCase().startsWith("NUMBER")) {
            if (scale > 0) {
                return "BigDecimal";
            }
            return "Integer";
        }
        if (fieldType.toUpperCase().startsWith("TIMESTAMP")) {
            return "TIMESTAMP";
        }if (fieldType.toUpperCase().startsWith("DATETIME")) {
            return "DATE";
        }
        if (fieldType.toUpperCase().startsWith("LONG")) {
            return "String";
        }
        if (fieldType.toUpperCase().startsWith("BIGINT UNSIGNED")) {
            return "Long";
        }
//        if (fieldType.toUpperCase().startsWith("BIGINT")) {
//            return "Long";
//        }
        if (fieldType.toUpperCase().startsWith("VARCHAR")) {
            return "String";
        }
        if (fieldType.toUpperCase().startsWith("TINYINT")) {
            return "Integer";
        }
        if (fieldType.toUpperCase().startsWith("INT")) {
            return "Integer";
        }
        if (fieldType.toUpperCase().startsWith("FLOAT")) {
            return "Float";
        }
        if (fieldType.toUpperCase().startsWith("DOUBLE")) {
            return "Double";
        }if (fieldType.toUpperCase().startsWith("DECIMAL")) {
            return "BigDecimal";
        }
        throw new RuntimeException("javaType not found! fieldType is " + fieldType);

    }

}
