package com.chendehe.creator;

import org.apache.commons.lang3.StringUtils;

import com.chendehe.entity.Table;
import com.google.common.base.CaseFormat;

import lombok.AllArgsConstructor;


/**
 * @author CDH
 * @since 2019/7/29 19:12
 */
@AllArgsConstructor
abstract class AbstractCreator {

    static final String MODEL_TEMP = "ModelTemplate";
    static final String DAO_TEMP = "DaoTemplate";
    static final String VO_TEMP = "VoTemplate";
    static final String SERVICE_TEMP = "ServiceTemplate";
    static final String IMPL_TEMP = "ImplTemplate";
    static final String CONTROLLER_TEMP = "ControllerTemplate";
    static final String SPLIT_CHAR = "#";
    static final String JAVA = ".java";

    Table table;

    //dao
    String getDaoName(String tableName) {
        return table.getDaoPrefix()
                .concat(CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL,
                        tableName.split(SPLIT_CHAR)[0].replaceAll(table.getTablePrefix(), StringUtils.EMPTY)))
                .concat(table.getDaoSuffix()).trim();
    }

    //model
    String getModelName(String tableName) {
        return table.getModelPrefix()
                .concat(CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL,
                        tableName.split(SPLIT_CHAR)[0].replaceAll(table.getTablePrefix(), StringUtils.EMPTY)))
                .concat(table.getModelSuffix()).trim();
    }

    //vo
    String getVoName(String tableName) {
        return table.getVoPrefix()
                .concat(CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL,
                        tableName.split(SPLIT_CHAR)[0].replaceAll(table.getTablePrefix(), StringUtils.EMPTY)))
                .concat(table.getVoSuffix()).trim();
    }

    //service
    String getServiceName(String tableName) {
        return table.getServicePrefix()
                .concat(CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL,
                        tableName.split(SPLIT_CHAR)[0].replaceAll(table.getTablePrefix(), StringUtils.EMPTY)))
                .concat(table.getServiceSuffix().trim());
    }
    //impl
    String getImplName(String tableName) {
        return table.getImplPrefix()
                .concat(CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL,
                        tableName.split(SPLIT_CHAR)[0].replaceAll(table.getTablePrefix(), StringUtils.EMPTY)))
                .concat(table.getImplSuffix().trim());
    }
    //controller
    String getControllerName(String tableName) {
        return table.getControllerPrefix()
                .concat(CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL,
                        tableName.split(SPLIT_CHAR)[0].replaceAll(table.getTablePrefix(), StringUtils.EMPTY)))
                .concat(table.getControllerSuffix().trim());
    }
}
