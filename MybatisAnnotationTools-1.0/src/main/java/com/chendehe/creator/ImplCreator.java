package com.chendehe.creator;

import com.chendehe.entity.Table;
import com.chendehe.utils.FileUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author CDH
 * @since 2019/7/29 11:01
 */

public class ImplCreator extends AbstractCreator {

    public ImplCreator(Table table) {
        super(table);
    }

    public void createImpl() {
        table.getTableFields()
            .forEach((longTableName, fields) -> FileUtils.writeToFile(
                FileUtils.createFile(table.getImplFolder().concat(getImplName(longTableName).concat(JAVA))),
                createImplFileContent(longTableName)));

        //
        String template = FileUtils.getTemplate("common/BaseDao.java");
        template = template.replaceAll("#<daoPackage>", table.getDaoPackage());
        template = template.replaceAll("#<createUser>", System.getProperty("user.name"));
        String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss"));
        template = template.replaceAll("#<createTime>", createTime);
        FileUtils.writeToFile(FileUtils.createFile(table.getImplFolder().concat("BaseDao.java")), template);
    }

    private String createImplFileContent(String longTableName) {
        final String[] tableNames = longTableName.split(SPLIT_CHAR);
        final String tableName = tableNames[0];
        String fileContent = FileUtils.getTemplate(IMPL_TEMP);
        final String tableComment = tableNames.length > 1 ? tableNames[1] : "";
        fileContent = fileContent.replaceAll("#<tableComment>", tableComment);
        fileContent = fileContent.replaceAll("#<tableName>", tableName);
        //service
        fileContent = fileContent.replaceAll("#<servicePackage>", table.getServicePackage());
        fileContent = fileContent.replaceAll("#<serviceName>", getServiceName(tableName));
        //impl
        fileContent = fileContent.replaceAll("#<implPackage>", table.getImplPackage());
        fileContent = fileContent.replaceAll("#<implName>", getImplName(tableName));
        //dao
        fileContent = fileContent.replaceAll("#<daoName>", getDaoName(tableName));
        fileContent = fileContent.replaceAll("#<daoPackage>", table.getDaoPackage());
        // model
        fileContent = fileContent.replaceAll("#<modelName>", getModelName(tableName));
        //vo
        fileContent = fileContent.replaceAll("#<voPackage>", table.getVoPackage());
        fileContent = fileContent.replaceAll("#<voName>", getVoName(tableName));

        fileContent = fileContent.replaceAll("#<createUser>", System.getProperty("user.name"));
        final String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss"));
        fileContent = fileContent.replaceAll("#<createTime>", createTime);
        return fileContent;
    }
}
