package com.chendehe.creator;

import com.chendehe.entity.Field;
import com.chendehe.entity.Table;
import com.chendehe.utils.FileUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * @author CDH
 * @since 2019/7/29 11:01
 */

public class ServiceCreator extends AbstractCreator {

    public ServiceCreator(Table table) {
        super(table);
    }

    public void createService() {
        table.getTableFields()
            .forEach((longTableName, fields) -> FileUtils.writeToFile(
                FileUtils.createFile(table.getServiceFolder().concat(getServiceName(longTableName).concat(JAVA))),
                createServiceFileContent(longTableName)));

        //
        String template = FileUtils.getTemplate("common/BaseDao.java");
        template = template.replaceAll("#<daoPackage>", table.getDaoPackage());
        template = template.replaceAll("#<createUser>", System.getProperty("user.name"));
        String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss"));
        template = template.replaceAll("#<createTime>", createTime);
        FileUtils.writeToFile(FileUtils.createFile(table.getServiceFolder().concat("BaseDao.java")), template);
    }

    private String createServiceFileContent(String longTableName) {
        final String[] tableNames = longTableName.split(SPLIT_CHAR);
        final String tableName = tableNames[0];
        String fileContent = FileUtils.getTemplate(SERVICE_TEMP);
        final String tableComment = tableNames.length > 1 ? tableNames[1] : "";
        fileContent = fileContent.replaceAll("#<tableComment>", tableComment);
        fileContent = fileContent.replaceAll("#<tableName>", tableName);
        //service
        fileContent = fileContent.replaceAll("#<servicePackage>", table.getServicePackage());
        fileContent = fileContent.replaceAll("#<serviceName>", getServiceName(tableName));
//        fileContent = fileContent.replaceAll("#<daoName>", getDaoName(tableName));
        // model
        fileContent = fileContent.replaceAll("#<modelName>", getModelName(tableName));
        //vo
        fileContent = fileContent.replaceAll("#<voPackage>", table.getVoPackage());
        fileContent = fileContent.replaceAll("#<voName>", getVoName(tableName));

        fileContent = fileContent.replaceAll("#<createUser>", System.getProperty("user.name"));
        final String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss"));
        fileContent = fileContent.replaceAll("#<createTime>", createTime);
        return fileContent;
    }
}
