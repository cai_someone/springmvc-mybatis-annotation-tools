package com.chendehe.creator;

import com.chendehe.entity.Table;
import com.chendehe.utils.FileUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author CDH
 * @since 2019/7/29 11:01
 */

public class ControllerCreator extends AbstractCreator {

    public ControllerCreator(Table table) {
        super(table);
    }

    public void createController() {
        table.getTableFields()
            .forEach((longTableName, fields) -> FileUtils.writeToFile(
                FileUtils.createFile(table.getControllerFolder().concat(getControllerName(longTableName).concat(JAVA))),
                createControllerFileContent(longTableName)));

        //
        String template = FileUtils.getTemplate("common/BaseDao.java");
//        template = template.replaceAll("#<daoPackage>", table.getDaoPackage());
        template = template.replaceAll("#<createUser>", System.getProperty("user.name"));
        String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss"));
        template = template.replaceAll("#<createTime>", createTime);
        FileUtils.writeToFile(FileUtils.createFile(table.getControllerFolder().concat("BaseDao.java")), template);
    }

    private String createControllerFileContent(String longTableName) {
        final String[] tableNames = longTableName.split(SPLIT_CHAR);
        final String tableName = tableNames[0];
        String fileContent = FileUtils.getTemplate(CONTROLLER_TEMP);
        final String tableComment = tableNames.length > 1 ? tableNames[1] : "";
        fileContent = fileContent.replaceAll("#<tableComment>", tableComment);
        fileContent = fileContent.replaceAll("#<tableName>", tableName);
        //service
        fileContent = fileContent.replaceAll("#<servicePackage>", table.getServicePackage());
        fileContent = fileContent.replaceAll("#<serviceName>", getServiceName(tableName));

        // model
        fileContent = fileContent.replaceAll("#<modelName>", getModelName(tableName));
        //vo
        fileContent = fileContent.replaceAll("#<voPackage>", table.getVoPackage());
        fileContent = fileContent.replaceAll("#<voName>", getVoName(tableName));
        //controller
        fileContent = fileContent.replaceAll("#<controllerName>",getControllerName(tableName));
        fileContent = fileContent.replaceAll("#<controllerPackage>",table.getControllerPackage());

        fileContent = fileContent.replaceAll("#<createUser>", System.getProperty("user.name"));
        final String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss"));
        fileContent = fileContent.replaceAll("#<createTime>", createTime);
        return fileContent;
    }
}
